package br.ucsal.bes20212.bd2.clinica.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table (name = "tab_uf")
public class UF {

	@Id
	@Column (name = "sigla", nullable=false, length = 2)
	private String sigla;

	@Column (name = "nome", nullable=false, length = 50)
	private String nome;

	@OneToMany(mappedBy = "ufcrm", fetch = FetchType.EAGER)
	private List<Medico> medicos;

	public UF() {
	}

	public UF(String sigla, String nome) {
		super();
		this.sigla = sigla;
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((sigla == null) ? 0 : sigla.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UF other = (UF) obj;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (sigla == null) {
			if (other.sigla != null)
				return false;
		} else if (!sigla.equals(other.sigla))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UF [sigla=" + sigla + ", nome=" + nome + "]";
	}

}
