package br.ucsal.bes20212.bd2.clinica.business;

import br.ucsal.bes20212.bd2.clinica.domain.Especialidade;
import br.ucsal.bes20212.bd2.clinica.domain.Medico;
import br.ucsal.bes20212.bd2.clinica.domain.UF;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

public class MedicoDAO {

    public static Medico criarMedico(EntityManager em, String nome, Date dataAdmissao, String email, List<String> telefones, String numeroCRM, UF ufCRM, List<Especialidade> especialidades){
        Medico medico = new Medico();
        medico.setNome(nome);
        medico.setDataAdmissao(dataAdmissao);
        medico.setTelefones(telefones);
        medico.setEmail(email);
        medico.setEspecialidades(especialidades);
        medico.setUfcrm(ufCRM);
        medico.setNumeroCRM(numeroCRM);

        em.persist(medico);

        return medico;
    }


}
