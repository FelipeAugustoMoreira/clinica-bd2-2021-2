package br.ucsal.bes20212.bd2.clinica.util;

import br.ucsal.bes20212.bd2.clinica.business.QuantidadeFuncionariosPorUFDTO;
import br.ucsal.bes20212.bd2.clinica.domain.Medico;
import br.ucsal.bes20212.bd2.clinica.domain.UF;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class ConsultasJPA {

    public static void listarMedicos(EntityManager em, String uf){
        String jpql = "select m from Medico m where m.ufcrm.sigla = :ufcrm order by m.nome asc";
        TypedQuery<Medico> queryMedico = em.createQuery(jpql, Medico.class);
        queryMedico.setParameter("ufcrm", uf);
        List<Medico> medicos = queryMedico.getResultList();

        for(Medico medico : medicos){
            System.out.println(medico);
        }
    }

    public static void quantidadePorUF(EntityManager em) {

        String jpql = "select new "+QuantidadeFuncionariosPorUFDTO.class.getCanonicalName()+"(m.ufcrm, count(*)) from Medico m";
        TypedQuery<QuantidadeFuncionariosPorUFDTO> queryDTO = em.createQuery(jpql, QuantidadeFuncionariosPorUFDTO.class);
        List<QuantidadeFuncionariosPorUFDTO> qtdDTO = queryDTO.getResultList();
        for(QuantidadeFuncionariosPorUFDTO q : qtdDTO) {
            System.out.println(q);
        }
    }

}
