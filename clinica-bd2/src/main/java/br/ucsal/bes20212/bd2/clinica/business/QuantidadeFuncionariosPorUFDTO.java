package br.ucsal.bes20212.bd2.clinica.business;

public class QuantidadeFuncionariosPorUFDTO {

    private String ufcrm;
    private Integer qtd;

    public QuantidadeFuncionariosPorUFDTO(String ufcrm, Integer qtd) {
        this.ufcrm = ufcrm;
        this.qtd = qtd;
    }

    public String getUfcrm() {
        return ufcrm;
    }

    public void setUfcrm(String ufcrm) {
        this.ufcrm = ufcrm;
    }

    public Integer getQtd() {
        return qtd;
    }

    public void setQtd(Integer qtd) {
        this.qtd = qtd;
    }

    @Override
    public String toString() {
        return "QuantidadeFuncionariosPorUFDTO{" +
                "sigla='" + ufcrm + '\'' +
                ", qtd=" + qtd +
                '}';
    }
}
