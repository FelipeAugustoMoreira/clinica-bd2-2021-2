package br.ucsal.bes20212.bd2.clinica.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tab_funcionario")
@SequenceGenerator(name = "matricula", sequenceName = "seq_matricula")
@Inheritance(strategy = InheritanceType.JOINED)
@DiscriminatorColumn( name = "FUNC")
public class Funcionario implements Serializable {

    @Id
    @Column(name = "matricula" )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_matricula")
    private Long matricula;

    @Column(name = "nome", columnDefinition = "varchar(40)", nullable = false)
    private String nome;

    @Temporal(TemporalType.DATE)
    @Column(name = "data_admissao", nullable = false)
    private Date dataAdmissao;

    @Column(name = "email", columnDefinition = "varchar(255)")
    private String email;

    @ElementCollection
    @CollectionTable(name = "tab_telefones")
    @Column(name = "telefone", length = 15, nullable = false)
    private List<String> telefones;

    public Funcionario(){}

    public Funcionario(String nome, Date dataAdmissao, String email, List<String> telefones) {

        this.nome = nome;
        this.dataAdmissao = dataAdmissao;
        this.email = email;
        this.telefones = telefones;
    }

    public Long getMatricula() {
        return matricula;
    }

    public void setMatricula(Long matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataAdmissao() {
        return dataAdmissao;
    }

    public void setDataAdmissao(Date dataAdmissao) {
        this.dataAdmissao = dataAdmissao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getTelefones() {
        return telefones;
    }

    public void setTelefones(List<String> telefones) {
        this.telefones = telefones;
    }

    @Override
    public String toString() {
        return "Funcionario{" +
                "matricula=" + matricula +
                ", nome='" + nome + '\'' +
                ", dataAdmissao=" + dataAdmissao +
                ", email='" + email + '\'' +
                ", telefones=" + telefones +
                '}';
    }
}
