package br.ucsal.bes20212.bd2.clinica.business;

import br.ucsal.bes20212.bd2.clinica.domain.UF;

import javax.persistence.EntityManager;

public class UFDAO {

    public static UF criarEstado(EntityManager em, String sigla, String nome){
        UF uf = new UF();
        uf.setNome(nome);
        uf.setSigla(sigla);

        em.persist(uf);

        return uf;
    }
}
