package br.ucsal.bes20212.bd2.clinica.util;

import br.ucsal.bes20212.bd2.clinica.business.MedicoDAO;
import br.ucsal.bes20212.bd2.clinica.business.UFDAO;
import br.ucsal.bes20212.bd2.clinica.domain.Enfermeiro;
import br.ucsal.bes20212.bd2.clinica.domain.Especialidade;
import br.ucsal.bes20212.bd2.clinica.domain.Medico;
import br.ucsal.bes20212.bd2.clinica.domain.UF;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Exemplo {
    public static void main(String[] args) {

        EntityManagerFactory emf = null;

        try {
            emf = Persistence.createEntityManagerFactory("clinica-bd2");
            EntityManager em = emf.createEntityManager();
            em.clear();

            popularBases(em);

            ConsultasJPA.listarMedicos(em, "BA");
            ConsultasJPA.quantidadePorUF(em);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if(emf != null) {
                emf.close();
            }
        }

    }

    public static void popularBases(EntityManager em) {
        em.getTransaction().begin();

        //Cria a data
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date dataDeAdmissao = new Date();
        try {
            dataDeAdmissao = dateFormat.parse("12-05-1979");
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //Cria lista de telefones
        List<String> telefonesTeste = new ArrayList<>();
        telefonesTeste.add("71999998888");
        telefonesTeste.add("71999998887");
        telefonesTeste.add("71999998886");
        telefonesTeste.add("71999998885");
        telefonesTeste.add("71999998884");

        //Cria Estados
        UF ba = UFDAO.criarEstado(em, "BA", "Bahia");
        UF sp = UFDAO.criarEstado(em,"SP", "São Paulo");
        UF rj = UFDAO.criarEstado(em,"RJ", "Rio de Janeiro");
        UF rs = UFDAO.criarEstado(em,"RS", "Rio Grande do Sul");

        //Cria lista de médicos
        List<Medico> medicos = new ArrayList<>();

        //Cria especialidades
        Especialidade pediatria = new Especialidade("Pediatria", medicos);
        Especialidade cirurgia = new Especialidade("Cirurgia", medicos);
        Especialidade oncologia = new Especialidade("Oncologia", medicos);

        //Cria lista de especialidades
        List<Especialidade> especialidades = new ArrayList<>();
        especialidades.add(pediatria);
        especialidades.add(cirurgia);
        especialidades.add(oncologia);

        Medico m1 = MedicoDAO.criarMedico(em,"Pedro Cajueiro", dataDeAdmissao,"cajueiro@med.com.br", telefonesTeste,"44571", ba,especialidades);
        Medico m2 = MedicoDAO.criarMedico(em,"Cláudio Neiva", dataDeAdmissao,"claudio@med.com.br", telefonesTeste,"656411", ba,especialidades);
        Medico m3 = MedicoDAO.criarMedico(em,"Torquato Asfalto", dataDeAdmissao,"torquato@med.com.br", telefonesTeste,"87812", sp,especialidades);
        Medico m4 = MedicoDAO.criarMedico(em,"Ivan Ivanovitch", dataDeAdmissao,"ivan@med.com.br", telefonesTeste,"546512", rj,especialidades);

        Enfermeiro e1 = new Enfermeiro("Maria Madalena", dataDeAdmissao, "maria@med.com.br", telefonesTeste,"45121");
        Enfermeiro e2 = new Enfermeiro("Carol Hathaway", dataDeAdmissao, "carol@med.com.br", telefonesTeste,"65451");
        Enfermeiro e3 = new Enfermeiro("Dedé Santana", dataDeAdmissao, "dede@med.com.br", telefonesTeste,"959663");
        Enfermeiro e4 = new Enfermeiro("Jim Kirk", dataDeAdmissao, "jim@med.com.br", telefonesTeste,"21574151");

        em.getTransaction().commit();
    }
}
