package br.ucsal.bes20212.bd2.clinica.domain;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "tab_enfermeiro", uniqueConstraints = {
        @UniqueConstraint(columnNames =  {"numero_registro_coren", "matricula"})
})
@PrimaryKeyJoinColumn(referencedColumnName = "matricula")
public class Enfermeiro extends Funcionario {

    @Column(name = "numero_registro_coren", columnDefinition = "varchar(10)", nullable = false)
    private String numeroRegistroCoren;

    public Enfermeiro(String nome, Date dataAdmissao, String email, List<String> telefones, String numeroRegistroCoren) {
        super(nome, dataAdmissao, email, telefones);
        this.numeroRegistroCoren = numeroRegistroCoren;
    }

    public String getNumeroRegistroCoren() {
        return numeroRegistroCoren;
    }

    public void setNumeroRegistroCoren(String numeroRegistroCoren) {
        this.numeroRegistroCoren = numeroRegistroCoren;
    }
}
