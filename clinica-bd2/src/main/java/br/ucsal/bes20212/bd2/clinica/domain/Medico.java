package br.ucsal.bes20212.bd2.clinica.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.*;

@Entity
@Table (name = "tab_medico", uniqueConstraints = {
		@UniqueConstraint(name= "un_medico_crm", columnNames = {"numero_crm"})})
@PrimaryKeyJoinColumn(referencedColumnName = "matricula")
public class Medico extends Funcionario {

	@Column (name = "numero_crm", length = 11, nullable=false)
	private String numeroCRM;


	@ManyToOne(optional = false, cascade = CascadeType.PERSIST)
	private UF ufcrm;
	
	@Column (name = "especialidades", unique=true)
	@ElementCollection
	@OneToMany(mappedBy = "nome", fetch = FetchType.EAGER)
	private List<Especialidade> especialidades;

	public Medico() {
		super();
	}

	public Medico(String nome, Date dataAdmissao, String email, List<String> telefones, String numeroCRM, UF ufcrm, List<Especialidade> especialidades) {
		super(nome, dataAdmissao, email, telefones);
		this.numeroCRM = numeroCRM;
		this.ufcrm = ufcrm;
		this.especialidades = especialidades;
	}

	public String getNumeroCRM() {
		return numeroCRM;
	}

	public void setNumeroCRM(String numeroCRM) {
		this.numeroCRM = numeroCRM;
	}

	public UF getUfcrm() {
		return ufcrm;
	}

	public void setUfcrm(UF ufcrm) {
		this.ufcrm = ufcrm;
	}

	public List<Especialidade> getEspecialidades() {
		return especialidades;
	}

	public void setEspecialidades(List<Especialidade> especialidades) {
		this.especialidades = especialidades;
	}

	@Override
	public String toString() {
		return  "Medico{" +
				"Matrícula: " +super.getMatricula() + '\'' +
				", Nome: " +super.getNome() + '\'' +
				", E-mail: " +super.getEmail() + '\'' +
				", Data de Admissão: " +super.getDataAdmissao() + '\'' +
				", Telefones: " +super.getTelefones() + '\'' +
				", numeroCRM: '" + numeroCRM + '\'' +
				", ufCRM: " + ufcrm +
				", especialidades: " + especialidades +
				'}';
	}
}
